const addTaskButton = document.querySelector('.add-task-button')
const createTaskTab = document.querySelector('.create-task-tab')
const taskList = document.querySelector('.task-list')

const clearUserTasks = () => taskList.innerHTML = ''

const renderUserTasks = () => {
    clearUserTasks()
    
    const userInfo = getCurrentUser()
    const { userId } = userInfo
    const userTasks = getUserTasks(userId)

    userTasks.forEach(task => {
        const tr = document.createElement('tr')
        const { id, title, date, status } = task
        const trContent = { id, title, date, status }

        for (propName in trContent) {
            const td = document.createElement('td')
            td.innerText = trContent[propName]
            tr.appendChild(td)
        }

        taskList.appendChild(tr)
    })
}

addTaskButton.addEventListener('click', () =>
    changeCssPropertyOf(createTaskTab, 'display', 'flex')
)
