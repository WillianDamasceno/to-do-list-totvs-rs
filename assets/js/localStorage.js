const forms = document.querySelector('.forms')
const tasks = document.querySelector('.tasks')

const getUsers = () => {
    if (!localStorage.users) localStorage.setItem('users', JSON.stringify([]))

    return JSON.parse(localStorage.users)
}

const getSpecificUser = userInfo => {
    const users = getUsers()
    const notAuthenticatedUser = users.filter(
        user => user.email === userInfo.email
    )

    const authenticatedUser =
        notAuthenticatedUser[0].password === userInfo.password
            ? notAuthenticatedUser[0]
            : false

    return authenticatedUser
}

const getCurrentUser = () => {
    const connectedUser = getConnectedUserInfo()
    return getSpecificUser(connectedUser)
}

const isEmptyOfUser = () => getUsers()[0] === undefined

const isExistingUser = userInfo => {
    const users = getUsers()
    const userEmail = userInfo.email

    return users.some(user => user.email === userEmail)
}

const createUser = userInfo => {
    const users = getUsers()

    if (!isExistingUser(userInfo)) {
        userInfo.userId = 0
        userInfo.connected = true

        if (!isEmptyOfUser()) {
            const lastUserId = users[users.length - 1].userId
            userInfo.userId = lastUserId + 1
        }

        users.push(userInfo)
        localStorage.setItem('users', JSON.stringify(users))
    } else
        console.warn(
            'This user already existis, add a warning in the form about it'
        )
}

const logInUser = userInfo => {
    if (isExistingUser(userInfo)) {
        const connectedUser = getSpecificUser(userInfo)

        if (connectedUser) {
            changeCssPropertyOf(forms, 'display', 'none')
            changeCssPropertyOf(tasks, 'display', 'block')

            saveCurrentUserInfo(userInfo)

            renderUserTasks()
        } else 
            throw Error('We could not authenticate your account!')
    } else 
        console.warn('This user does not exists, please Sign Up')
}

const logOutUser = () => {
    const avatarChecker = document.querySelector('[data-js="avatar-checker"')

    avatarChecker.checked = false
    
    clearCurrentUserInfo()

    changeCssPropertyOf(forms, 'display', 'flex')
    changeCssPropertyOf(tasks, 'display', 'none')
}

const signUpUser = userInfo => {
    if (!isExistingUser(userInfo)) {
        createUser(userInfo)
        logInUser(userInfo)
    } else
        console.warn(
            'This user already existis, add a warning in the form about it'
        )
}
