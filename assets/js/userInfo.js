const getConnectedUserInfo = () => {
    try {
        return JSON.parse(localStorage.connectedUser)
    } catch (error) {
        localStorage.connectedUser = JSON.stringify({})
    }
}

const saveCurrentUserInfo = userInfo => {
    const { email, password } = userInfo
    const userInfoJson = JSON.stringify({
        email,
        password
    })

    localStorage.connectedUser = userInfoJson
}

const clearCurrentUserInfo = () =>
    localStorage.removeItem('connectedUser')

document.addEventListener('DOMContentLoaded', () => {
    const connectedUserInfo = getConnectedUserInfo()
    
    if (connectedUserInfo) {
        const connectedUserInfoJson = connectedUserInfo
        logInUser(connectedUserInfoJson)
    }
})
