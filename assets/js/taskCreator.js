const createTaskCancelButton = document.querySelector(
    '.create-task-cancel-button'
)
const createTaskSaveButton = document.querySelector('.create-task-save-button')

createTaskCancelButton.addEventListener('click', () => {
    const createTaskTabChildren = createTaskTab.children[0]
    const createTaskTabInputs = getChildrenFromElement(createTaskTabChildren, [
        'input',
        'textarea',
    ])

    createTaskTabInputs.forEach(input => (input.value = ''))

    changeCssPropertyOf(createTaskTab, 'display', 'none')
})

createTaskSaveButton.addEventListener('click', () => {
    const createTaskTabChildren = createTaskTab.children[0]
    const createTaskTabInputs = getChildrenFromElement(createTaskTabChildren, [
        'input',
        'textarea',
    ])

    if (isEveryInputFilled(createTaskTabInputs)) {
        const userInfo = getCurrentUser()
        const { userId } = userInfo

        const [title, description, date] = createTaskTabInputs.map(
            input => input.value
        )

        const task = { userId, title, description, date, status: 0 }

        createTaskTabInputs.forEach(input => (input.value = ''))

        saveTask(task)

        renderUserTasks(userId)

        changeCssPropertyOf(createTaskTab, 'display', 'none')
    }
})
