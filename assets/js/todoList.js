const createTodoList = () => (localStorage.todoList = JSON.stringify([]))

const updateTodoList = updatedList =>
    (localStorage.todoList = JSON.stringify(updatedList))

const getTodoList = () => localStorage.todoList || createTodoList()

const saveTask = ({ userId, title, description, date, status }) => {
    const todoList = JSON.parse(getTodoList())
    let id = 0

    if (todoList[0]) {
        id = todoList[todoList.length - 1].id + 1
    }

    const task = { id, userId, title, description, date, status }

    todoList.push(task)

    updateTodoList(todoList)
}

const getUserTasks = userId => {
    const todoList = JSON.parse(getTodoList())

    const userTasks = todoList.filter(task => task.userId === userId)

    return userTasks
}

const getUserTask = (taskId, userId) => {
    const todoList = JSON.parse(getTodoList())

    const userTask = todoList.filter(task => task.id === taskId && task.userId === userId)

    return userTask[0]
}

const updateTask = taskInfo => {
    const userInfo = getCurrentUser()
    const { userId } = userInfo
    const todoList = JSON.parse(getTodoList())

    taskInfo.userId = userId
    
    const tasks = getUserTasks(userId)
    const updatedUserTasks = tasks.filter(task => task.id !== taskInfo.id)
    updatedUserTasks.push(taskInfo)

    const tasksFromOtherUsers = todoList.filter(task => task.userId !== userId)
    const updatedTasks = [...updatedUserTasks, ...tasksFromOtherUsers]

    updateTodoList(updatedTasks)

    renderUserTasks()
}

const deleteUserTask = taskId => {
    const userInfo = getCurrentUser()
    const { userId } = userInfo
    const todoList = JSON.parse(getTodoList())

    const tasks = getUserTasks(userId)
    const updatedUserTasks = tasks.filter(task => task.id !== taskId)
    
    const tasksFromOtherUsers = todoList.filter(task => task.userId !== userId)
    const updatedTasks = [...updatedUserTasks, ...tasksFromOtherUsers]

    updateTodoList(updatedTasks)

    renderUserTasks()
}
