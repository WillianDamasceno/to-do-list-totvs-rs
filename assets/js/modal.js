const modal = document.querySelector('.modal')
const linkToModal = document.querySelector('[data-js="link-to-modal"]')

const modalDeleteButton = document.querySelector(
    '[data-js="modal-delete-button"]'
)
const modalCancelButton = document.querySelector(
    '[data-js="modal-cancel-button"]'
)
const modalSaveButton = document.querySelector('[data-js="modal-save-button"]')

const deletingConfirmationMessage = document.querySelector(
    '.deleting-confirmation-message'
)

modalDeleteButton.addEventListener('click', () => {
    changeCssPropertyOf(deletingConfirmationMessage, 'opacity', '1')
    changeCssPropertyOf(deletingConfirmationMessage, 'pointer-events', 'auto')
})

deletingConfirmationMessage.addEventListener('click', ({target}) => {
    if (target.innerText === 'Cancel') {
        changeCssPropertyOf(deletingConfirmationMessage, 'opacity', '0')
        changeCssPropertyOf(deletingConfirmationMessage, 'pointer-events', 'none')
    } else if (target.innerText === 'Delete') {
        const taskId = Number(linkToModal.dataset.taskId)
        deleteUserTask(taskId)

        modalCancelButton.children[0].click()
        
        changeCssPropertyOf(deletingConfirmationMessage, 'opacity', '0')
        changeCssPropertyOf(deletingConfirmationMessage, 'pointer-events', 'none')
    }
})

modalSaveButton.addEventListener('click', () => {
    const modalContent = modal.children[0]
    const modalInputs = getChildrenFromElement(modalContent, [
        'input',
        'textarea',
    ])
    const [title, description, date] = modalInputs.map(input => input.value)
    const taskId = Number(linkToModal.dataset.taskId)

    const updatedTaskInfo = { id: taskId, title, description, date, status: 0 }

    updateTask(updatedTaskInfo)
})

taskList.addEventListener('mouseover', ({ target }) => {
    const clickedTaskRow = target.parentElement
    const taskContent = [...clickedTaskRow.children].map(td => td.innerText)

    linkToModal.dataset.modal = taskContent
})

taskList.addEventListener('click', () => {
    const userInfo = getCurrentUser()
    const { userId } = userInfo

    const modalContent = modal.children[0]
    const [
        ,
        titleModelField,
        ,
        descriptionModelField,
        ,
        dateModelField,
    ] = modalContent.children

    const taskInfo = linkToModal.dataset.modal.split(',')
    const [id, title, date, status] = taskInfo

    const description = getUserTask(Number(id), userId).description

    titleModelField.value = title
    descriptionModelField.value = description
    dateModelField.value = date

    linkToModal.dataset.taskId = id

    linkToModal.click()
})
