const formContainer = document.querySelector('.form-container')
const loginForm = document.querySelector('.login-form')
const signupForm = document.querySelector('.signup-form')

const loginTab = document.querySelector('[data-js="login-tab"]')
const loginLabelTab = document.querySelector('.login-tab')

const signupTab = document.querySelector('[data-js="signup-tab"]')
const signupLabelTab = document.querySelector('.signup-tab')

const switchAuthenticationOption = ({ target }) => {
    if (target.type === 'radio') {
        if (target.dataset.js === loginLabelTab.classList[0]) {
            changeCssPropertyOf(loginForm, 'display','block')
            changeCssPropertyOf(signupForm, 'display', 'none')
        } else {
            changeCssPropertyOf(loginForm, 'display', 'none')
            changeCssPropertyOf(signupForm, 'display', 'block')
        }
    }
}

const renderIncompleteFieldWarning = form => {
    if (form.children[form.children.length - 1].localName !== 'div') {
        const incompleteFieldWarning = document.createElement('div')
        const pTag = document.createElement('p')

        incompleteFieldWarning.classList.add('form-incomplete-field-warning')
        incompleteFieldWarning.appendChild(pTag)

        pTag.innerHTML = 'Please, complete all the fields to continue!'

        form.appendChild(incompleteFieldWarning)
    }
}

const getUserInfoFromForm = (submitButton, form) => {
    if (submitButton.localName === 'button') {
        const inputs = getChildrenFromElement(form, ['input'])

        if (isEveryInputFilled(inputs)) {
            const userInfo = inputs.reduce(
                (accumulator, input) => (
                    (accumulator[input.dataset.js] = input.value), accumulator
                ),
                {}
            )

            return userInfo
        }
    }
}

formContainer.addEventListener('change', switchAuthenticationOption)

loginForm.addEventListener('click', function ({ target }) {
    const userInfo = getUserInfoFromForm(target, this)

    if (userInfo) 
        logInUser(userInfo)
    else if (target.localName === 'button')
        renderIncompleteFieldWarning(this)
})

signupForm.addEventListener('click', function ({ target }) {
    const userInfo = getUserInfoFromForm(target, this)

    if (userInfo)
        signUpUser(userInfo)
    else if (target.localName === 'button')
        renderIncompleteFieldWarning(this)
})
