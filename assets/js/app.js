const getChildrenFromElement = (element, childTagNames = []) => {
    const elementChildren = Array.from(element.children)
    const filteredChildren = elementChildren.filter(childElement =>
        childTagNames.some(tagName => tagName === childElement.localName)
    )

    return filteredChildren
}

const isEveryInputFilled = (inputs = []) =>
    inputs.every(input => input.value !== '')

const changeCssPropertyOf = (element, attributeName, newValue) =>
    (element.style[attributeName] = newValue)
