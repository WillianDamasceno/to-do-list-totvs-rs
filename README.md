<h1>
    <img src="./assets/images/logo/logo-totvs-rs-gray.png" width="40px">
    TOTVS TO DO List
</h1>

## Web App para organizar suas tarefas em uma tabela.

## 📝 Sobre
Este Web App está sendo desenvolvido para um processor de seleção.

---

## 💻 Tecnologias utilizadas

- HTML5
- CSS3
- JavaScript

---

## 📁 Como baixar o projeto

- Clique no botão de **Download**, e selecione o formato **Zip**, e então selecione a pasta onde quer que o projeto seja clonado.

<img src="./assets/images/reedmeImages/download-project.png">

- Em seguida extraia o arquivo baixado, e abra a pasta gerada.

<img src="./assets/images/reedmeImages/extract.png">


- E então basta **clicar duas vezes no arquivo index.html** e a aplicação estará redando na sua máquina.

<img src="./assets/images/reedmeImages/open-index.png">

---

Desenvolvido por Willian Damasceno
